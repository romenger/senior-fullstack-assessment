<?php

namespace Scale;

/**
 * This interface defines the methods that a
 * TreeHandler should implement
 */
interface TreeHandlerInterface
{
	/**
	 * Return the Node from a tree which has the given ID
	 *
	 * @param  Node    $tree the root node of a tree
	 * @param  integer $id   the id to find
	 * @return Node
	 */
	public function findNodeById(Node $tree, $id);

	/**
	 * Return an array of Nodes that form the path from the
	 * root of a tree to the Node with the given id.
	 *
	 * Example:
	 *
	 * Using the example diagram, if we were given id #8, this
	 * method would return: [Node1, Node2, Node9, Node7, Node8]
	 *
	 * @param  Node    $tree the root node of a tree
	 * @param  integer $id   the id to find
	 * @return Node[]
	 */
	public function getPathToId(Node $tree, $id);
}
