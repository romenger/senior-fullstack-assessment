<?php

namespace Scale;

/**
 * This class handles creating a sample tree for testing
 */
class SampleTree
{
	/**
	 * Create a new sample tree (Node)
	 *
	 * @return Node
	 */
	public function create()
	{
		// create all of our nodes
		$node1 = new Node(1);
		$node2 = new Node(2);
		$node3 = new Node(3);
		$node4 = new Node(4);
		$node5 = new Node(5);
		$node6 = new Node(6);
		$node7 = new Node(7);
		$node8 = new Node(8);
		$node9 = new Node(9);
		$node10 = new Node(10);
		$node11 = new Node(11);

		// build branches
		$node1->children[] = $node2;
		$node1->children[] = $node10;
		$node1->children[] = $node3;

		$node2->children[] = $node5;
		$node2->children[] = $node9;

		$node9->children[] = $node6;
		$node9->children[] = $node7;

		$node7->children[] = $node8;

		$node3->children[] = $node11;
		$node3->children[] = $node4;

		return $node1;
	}
}
