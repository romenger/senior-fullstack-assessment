<?php

namespace Scale;

/**
 * This interface defines the methods to save and load
 * a tree to/from the filesystem
 */
interface TreeStorageInterface
{
	/**
	 * Save a tree (Node) to the filesystem
	 *
	 * @param  string $path the absolute path to save to
	 * @param  Node   $tree the root Node of a tree
	 * @return void
	 */
	public function save($path, Node $tree);

	/**
	 * Load a tree (Node) from the filesystem
	 *
	 * @param  string $path the absolute path to load tree from
	 * @return Node         root Node for the loaded tree
	 */
	public function load($path);
}
