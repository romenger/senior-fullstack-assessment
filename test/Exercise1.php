<?php

/**
 * This is a test script for Exercise #1 in which you should:
 *
 * 1. Create a MyTreeHandler class which implements Scale\TreeHandlerInterface
 * 2. Get the tests in this file to pass
 */

require(dirname(__FILE__) . "/../src/autoload.php");

$tree = (new Scale\SampleTree)->create();
$handler = new \MyTreeHandler();

echo "Exercise#1 - findNodeById():\r\n";

$node = $handler->findNodeById($tree, 4);

if (!$node || $node->id != 4) {
	echo "failed\r\n";
} else {
	echo "passed\r\n";
}

echo "\r\nExercise#1 - getPathToId():\r\n";

$nodes = $handler->getPathToId($tree, 8);

if (is_array($node) &&
	count($nodes) == 5 &&
	$nodes[0]->id == 1 &&
	$nodes[1]->id == 2 &&
	$nodes[2]->id == 9 &&
	$nodes[3]->id == 7 &&
	$nodes[4]->id == 8) {
	echo "passed\r\n";
} else {
	echo "failed\r\n";
}
